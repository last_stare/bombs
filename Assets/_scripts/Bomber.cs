﻿using UniRx;
using UnityEngine;

public class Bomber : MonoBehaviour
{
    private CompositeDisposable disposables;
    private GameObject          bombObject;

    private void Awake() =>  bombObject = Resources.Load("bombObj") as GameObject;
    
    private void OnEnable()
    {
        disposables = new CompositeDisposable();
        Observable.Timer(System.TimeSpan.FromSeconds(0.75f)) //подписываем на регулярное сбрасывание бомб
                         .Repeat()
                         .Subscribe(_ => { Bombing(); })
                         .AddTo(disposables);
    }

    /// <summary>
    /// Реализем сбрасывание бомбы
    /// </summary>
    private void Bombing() => Instantiate(bombObject, Pointer.RandomNavmeshPont(15, Vector3.zero, 5f), Quaternion.identity);
    
    private void OnDisable() =>  disposables.Dispose();
    
}
