﻿
using UniRx;

interface IDamagable 
{
    ReactiveProperty<float> health { get;  set; }
    void ApplyDamage(float damage);
}
