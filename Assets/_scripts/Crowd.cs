﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Crowd : MonoBehaviour
{
    public  static List<Unit>           Units = new List<Unit>();
    private         CompositeDisposable disposables;
    private         GameObject          unitObj;
   
    private void Awake() => unitObj = Resources.Load("unitObj") as GameObject;
    
    private void OnEnable()
    {
        disposables = new CompositeDisposable();
        Observable.Timer(System.TimeSpan.FromSeconds(1f))       //Периодичекое создание юнитов
                          .Repeat()                           
                          .Subscribe(_ => { Spawn(); })
                          .AddTo(disposables);

    }
    /// <summary>
    /// Созжаём новых юнитов
    /// </summary>
    private void Spawn()
    {
        if (Units.Count > 49) return;
        Unit newUnit =  Instantiate(unitObj, Pointer.RandomNavmeshPont(15, Vector3.zero), Quaternion.identity).GetComponent<Unit>();
        Units.Add(newUnit);
    }
    

    private void OnDisable() =>  disposables.Dispose();
    


}
