﻿using UnityEngine;
using UniRx;
using System.Linq;

public class Bomb : MonoBehaviour
{
    public enum BombSize { light, medium, heavy }
 
    private float force;
    private float exploRadius;
    private Color col;
    private void Start() => PeformType();
    
    /// <summary>
    /// Рандомно определяем тип бомбы
    /// </summary>
    private void PeformType()
    {
        BombSize size = (BombSize)Random.Range(0, 3);
        switch (size)
        {
            case BombSize.light:
                force = 25;
                exploRadius = 2;
                col = Color.green;
                break;
            case BombSize.medium:
                force = 50;
                exploRadius = 3;
                col = Color.yellow;
                break;
            case BombSize.heavy:
                force = 75;
                exploRadius = 5;
                col = Color.red;
                break;
        }
        GetComponent<MeshRenderer>().material.color = col;
    }

    /// <summary>
    /// Реализуем взрыа при ударе
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        Explosion();
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Взрыв и анализ рядом стоящих объектов
    /// </summary>
    private void Explosion()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, exploRadius);
        var unitFiltered = hitColliders.ToList().Where(x =>x.gameObject.layer == 9);    //убираем всё кроме юнитов
        var valid = unitFiltered.Where(x => CheckNoObstacle(x.transform));              //проаеряем преграды
        foreach (var victim in valid)
        {
            victim.GetComponent<Unit>().ApplyDamage(force);                             //наносим повреждение
        }
    }

    /// <summary>
    /// Проверка преград
    /// </summary>
    /// <param name="victim"></param>
    /// <returns></returns>
    private bool CheckNoObstacle(Transform victim)
    {
        RaycastHit hit;
        Vector3 direction = victim.position - transform.position;
        if (Physics.Raycast(transform.position, direction, out hit))
        {
            if (hit.collider.transform == victim) return true;
        }
        return false;
    }
}
