﻿using UniRx;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public class Unit : MonoBehaviour, IMovable, IDamagable
{ 
    private NavMeshAgent        agent;
    private Text                hpText;
    private CompositeDisposable disposables;
    public ReactiveProperty<float> health { get; set; }

    private void OnEnable()
    {
        disposables = new CompositeDisposable();
        Observable.Timer(System.TimeSpan.FromSeconds(4))        //Периодическое направление юнитов
                  .Repeat()
                  .Subscribe(_ => { Go(); })
                  .AddTo(disposables);
    }
    
    private void OnDisable() =>  disposables.Dispose();

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        hpText = GetComponentInChildren<Text>();
        health = new ReactiveProperty<float>(100);
        health.ObserveEveryValueChanged(x => x.Value) //подпись на reactive priperty
           .Subscribe(xs =>                                                             
           {
               hpText.text = health.ToString();
           }).AddTo(disposables);
    }

    /// <summary>
    /// Имплементация интерфейса IMovable
    /// </summary>
    /// <param name="point"></param>
    public void MoveTo(Vector3 point) => agent.SetDestination(point);

    /// <summary>
    /// Имплементация интерфейса IDamagable
    /// </summary>
    /// <param name="damage"></param>
    public void ApplyDamage(float damage)
    {
        health.Value -= damage;
        if (health.Value <= 0)
        {
            Crowd.Units.Remove(this);
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Задаём путь
    /// </summary>
    private void Go() => MoveTo(Pointer.RandomNavmeshPont(15, transform.position));

}
